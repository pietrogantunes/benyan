---
title: "Motivação para o Tema"
date: 2022-09-29T17:48:47-03:00
draft: false
---

Com a decisão de fazer um curso de graduação fora do meu estado, veio a necessidade de me mudar e, assim, passar a morar longe da minha família e dos meus cachorros de estimação. A distância muitas vezes pode ser difícil de se lidar, visto que em certos momentos a saudade é muito forte. Nesse site, tenho a intenção de falar um pouco mais sobre os ambos os meus pets e expor um pouco do meu amor por eles. O nome do site se dá por um união dos nomes "Benta" e "Ryan", resultando em "Benyan".