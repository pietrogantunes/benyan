---
title: "Benta"
date: 2022-09-29T17:58:19-03:00
draft: false
---

Raça: Lhasa Apso

Data de nascimento: 19 de janeiro de 2007 (15 anos)

Hobby: Comer e dormir

Alimento favorito: Cenoura

Defeito: Não gosta de abraço

![Alt text](/images/benta_banana.jpeg "benta banana")
![Alt text](/images/benta_abraco.png "benta abraço")
