---
title: "Chica"
date: 2021-09-18T23:28:40-03:00
draft: false
---

A Chica foi uma gatinha de rua que ficou em minha casa por uns três meses até acharmos um lar definitivo pra ela. Ela não é meu pet atualmente, mas foi por um momento, e por isso está ganhando uma página própria também.

![Alt text](/images/chica_bonita.jpeg "chica bonita")